﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ICA_7.Models;

namespace ICA_7.Controllers
{
    public class CustomerController : Controller
    {
        static List<CustomerModel> customerList = new List<CustomerModel>();
        static Tuple<List<CustomerModel>, CustomerModel> customerTuple;
        // GET: Customer
        public ActionResult Index()
        {
            if(customerList.Count != 3)
            {
                customerList.Add(new CustomerModel(1, "Bob", 12));
                customerList.Add(new CustomerModel(2, "Joe", 14));
                customerList.Add(new CustomerModel(3, "Sue", 16));
            }
            customerTuple = new Tuple<List<CustomerModel>, CustomerModel>(customerList, customerList.ElementAt(0));
            return View("Customer",customerTuple);
        }
        
        public ActionResult OnSelectCustomer(string customer)
        {
            switch (customer)
            {
                case "1":
                    customerTuple = new Tuple<List<CustomerModel>, CustomerModel>(customerList, customerList.ElementAt(0));
                    break;
                case "2":
                    customerTuple = new Tuple<List<CustomerModel>, CustomerModel>(customerList, customerList.ElementAt(1));
                    break;
                case "3":
                    customerTuple = new Tuple<List<CustomerModel>, CustomerModel>(customerList, customerList.ElementAt(2));
                    break;
            }

                return View("_CustomerDetails", customerTuple);
        }
    }
}